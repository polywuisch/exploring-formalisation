# Exploring Formalisation

Lean 3 sources for the book project "Exploring Formalisation. A Primer in Human-Readable Mathematics in Lean 3 with Examples from Simplicial Topology" by Clara Löh (Springer, 2022, to appear).

[Homepage of the book project](http://loeh.app.ur.de/exploring-formalisation)

## License 

Copyright (c) 2022 Clara Löh. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Author: Clara Löh.

/-
Copyright (c) 2022 Clara Löh. All rights reserved.
Released under Apache 2.0 license as described in the file LICENSE.txt.
Author: Clara Löh.
-/

import tactic          -- standard proof tactics
import data.set        -- basics on sets
import data.set.finite -- basics on finite sets
import data.finset     -- type-level finite sets
import simplicial_complex -- basics on simplicial complexes

open classical  -- we work in classical logic

/-
# Simplicial maps
-/

/- Simplicial maps are maps between the underlying base types 
   that map simplices to simplices.
-/   
@[simp]
def is_simplicial_map 
    {a : Type*} {b : Type*} [decidable_eq b]
    (X : simplicial_complex a)
    (Y : simplicial_complex b)
    (f : a → b)
:= ∀ s, s ∈ X.simplices → finset.image f s ∈ Y.simplices    

structure simplicial_map
   {a : Type*} {b : Type*} [decidable_eq b]
   (X : simplicial_complex a) 
   (Y : simplicial_complex b)
:= mk :: (map : a → b)
         (is_simplicial : is_simplicial_map X Y map)

/-
# Simplicial maps on vertices
-/

/- In order to prove that simplicial maps 
   map vertices to vertices, we first show: 
   One can go back and forth between vertices 
   and singletons that are simplices.
-/
lemma vertex_to_singleton
    {a : Type*}
    (X : simplicial_complex a)
    (x : a)
    (x_in_X : x ∈ vertices X)
  : {x} ∈ X.simplices
:=
begin
  -- As x is a vertex, x is contained in a simplex s of X.
  -- Therefore, {x} ⊆ s.
  rcases x_in_X with ⟨ s, ⟨ s_in_SX, x_in_s⟩ ⟩,
  have x_sub_s : {x} ⊆ s, 
       by finish,

  -- As the set of simplices of X is closed under subsets, 
  -- also {x} is a simplex of X.
  exact X.subset_closed s s_in_SX {x} x_sub_s,
end     

lemma singleton_to_vertex
    {a : Type*}
    (X : simplicial_complex a)
    (x : a)
    (x_in_SX : {x} ∈ X.simplices)
  : x ∈ vertices X
:=
begin
  -- {x} witnesses that x is contained in a simplex 
  -- and thus is a vertex
  simp only[vertices, set.mem_def, set.mem_set_of_eq],
  use {x},
  finish,
end  

-- Simplicial maps map vertices to vertices.
lemma simplicial_map_on_vertices
    {a : Type*} {b : Type*} [decidable_eq b]
    (X : simplicial_complex a)
    (Y : simplicial_complex b)
    (f : simplicial_map X Y)
    (x : a)
    (x_in_X : x ∈ vertices X)
  : f.map x ∈ vertices Y
:=
begin
  simp only[vertices, set.mem_def, set.mem_set_of_eq],
  
  -- We use t := {f(x)} as a witness.
  let t : finset b := {f.map x},
  use t,

  -- Thus, it suffices to show that t is a simplex of Y
  have t_in_SY : t ∈ Y.simplices, from
  begin
    have x_in_SX : {x} ∈ X.simplices,
         by exact vertex_to_singleton X x x_in_X,
    have fx_is_t : finset.image f.map {x} = t, 
         by finish,
    have fx_in_SY : finset.image f.map {x} ∈ Y.simplices,
         by exact (f.is_simplicial {x} x_in_SX),
    show _, by finish,
  end,

  -- ... and that f(x) ∈ t.
  have fx_in_t : f.map x ∈ t, 
       by finish,

  show _, by exact ⟨t_in_SY, fx_in_t⟩,
end

/-
# Dimension monotonicity
-/

-- The dimension of simplices does not increase 
-- under simplicial maps.
lemma simplicial_map_dim_mono
    {a : Type*} {b : Type*} [decidable_eq b]
    (X : simplicial_complex a)
    (Y : simplicial_complex b)
    (f : simplicial_map X Y)
    (s : finset a)
    (s_in_SX : s ∈ X.simplices)
  : dim (finset.image f.map s) ≤ dim s
:=
begin
  calc dim (finset.image f.map s) 
         = finset.card (finset.image f.map s) - 1 
         : by unfold dim
     ... ≤ finset.card s - 1
         : by simp[finset.card_image_le]
     ... ≤ dim s 
         : by unfold dim,
end

/-
# Examples
-/         

-- The identity map is simplicial.
lemma id_is_simplicial_map 
    {a : Type*} [decidable_eq a]
    (X : simplicial_complex a)
  : is_simplicial_map X X (λ x : a, x)
:=
begin
  simp,
end

def id_simplicial_map
    {a : Type*} [decidable_eq a]
    (X : simplicial_complex a)
  : simplicial_map X X 
:= simplicial_map.mk 
     (λ x : a, x)
     (id_is_simplicial_map X)    

-- Constant maps are simplicial
lemma const_is_simplicial 
      {a : Type*} {b :  Type*} [decidable_eq b]
      (X : simplicial_complex a)
      (Y : simplicial_complex b)
      (y_0 : b)
      (y0_vertex : y_0 ∈ vertices Y)
    : is_simplicial_map X Y (λ x : a, y_0)
:=
begin
  -- As y_0 is a vertex of Y, the set {y_0} is a simplex of Y  
  have y0_in_SY : {y_0} ∈ Y.simplices, 
       by exact vertex_to_singleton Y y_0 y0_vertex,

  -- Setup for the main argument
  assume s : finset a,
  assume s_in_SX : s ∈ X.simplices,
  
  let f  := λ x : a, y_0,
  let fs := finset.image f s,

  -- We have f(s) ⊆ {y_0}
  have fs_sub_y0 : fs ⊆ {y_0}, from
  begin
    assume y,
    assume y_in_fs : y ∈ fs,
    show y ∈ {y_0}, by finish,
  end,

  -- and thus f(s) is a simplex of Y.
  show fs ∈ Y.simplices, 
       by exact Y.subset_closed {y_0} y0_in_SY fs fs_sub_y0,
end  

def const_simplicial_map
    {a : Type*} {b :  Type*} [decidable_eq b]
    (X : simplicial_complex a)
    (Y : simplicial_complex b)
    (y_0 : b)
    (y0_vertex : y_0 ∈ vertices Y)
  : simplicial_map X Y
:= simplicial_map.mk 
     (λ x : a, y_0)
     (const_is_simplicial X Y y_0 y0_vertex)  

-- Compositions of simplicial maps are simplicial
lemma is_simplicial_comp 
    {a : Type*} {b : Type*} {c : Type*} [decidable_eq b] [decidable_eq c]
    {X : simplicial_complex a}
    {Y : simplicial_complex b}
    {Z : simplicial_complex c}
    (f : a → b)
    (f_simpl : is_simplicial_map X Y f)
    (g : b → c)
    (g_simpl : is_simplicial_map Y Z g)
  : is_simplicial_map X Z (g ∘ f)
:=
begin
  assume s : finset a,
  assume s_in_SX : s ∈ X.simplices,

  let t : finset b := finset.image f s,

  have t_in_SY : t ∈ Y.simplices,
       by {apply (f_simpl s), apply s_in_SX}, -- or just: tauto},

  have gfs_in_SZ : finset.image g t ∈ Z.simplices,
       by {apply (g_simpl t), apply t_in_SY},

  show finset.image (g ∘ f) s ∈ Z.simplices, 
    by calc finset.image (g ∘ f) s 
          = finset.image g (finset.image f s) 
          : by exact finset.image_image.symm
      ... = finset.image g t
          : by refl
      ... ∈ Z.simplices
          : by exact gfs_in_SZ,     
end     

def simplicial_map.comp 
    {a : Type*} {b : Type*} {c : Type*} [decidable_eq b] [decidable_eq c]
    {X : simplicial_complex a}
    {Y : simplicial_complex b}
    {Z : simplicial_complex c}
    (f : simplicial_map X Y)
    (g : simplicial_map Y Z)
  : simplicial_map X Z
:= simplicial_map.mk 
     (g.map ∘ f.map)
     (is_simplicial_comp f.map f.is_simplicial g.map g.is_simplicial)     

/-
# Simplicial isomorphisms
-/

-- A simplicial map is a simplicial isomorphism
-- if it admits an inverse simplicial map.
@[simp]
def is_inverse_simplicial_iso
    {a : Type*} {b : Type*} [decidable_eq a] [decidable_eq b]
    {X : simplicial_complex a}
    {Y : simplicial_complex b}
    (f : simplicial_map X Y)
    (g : simplicial_map Y X)
:= simplicial_map.comp f g = id_simplicial_map X
 ∧ simplicial_map.comp g f = id_simplicial_map Y

@[simp]
def is_simplicial_iso 
    {a : Type*} {b : Type*} [decidable_eq a] [decidable_eq b]
    {X : simplicial_complex a}
    {Y : simplicial_complex b}
    (f : simplicial_map X Y)
:= ∃ g : simplicial_map Y X, is_inverse_simplicial_iso f g

-- For example, the identity map is a simplicial isomorphism
-- because it is its own inverse
lemma id_is_simplicial_iso
    {a : Type*} [decidable_eq a]
    (X : simplicial_complex a)
  : is_simplicial_iso (id_simplicial_map X)
:=
begin
  use id_simplicial_map X,
  show is_inverse_simplicial_iso (id_simplicial_map X) (id_simplicial_map X), 
       by finish,
end
